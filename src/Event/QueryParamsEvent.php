<?php

namespace Drupal\search_api_opensearch\Event;

use Drupal\search_api\Query\QueryInterface;

/**
 * Event triggered when search params are built.
 */
class QueryParamsEvent extends BaseParamsEvent {

  public function __construct(
    string $indexName,
    array $params,
    protected readonly QueryInterface $query,
  ) {
    parent::__construct($indexName, $params);
  }

  /**
   * Gets the query.
   *
   * @return \Drupal\search_api\Query\QueryInterface
   *   Query object.
   */
  public function getQuery(): QueryInterface {
    return $this->query;
  }

}
