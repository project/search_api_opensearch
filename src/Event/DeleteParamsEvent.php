<?php

namespace Drupal\search_api_opensearch\Event;

/**
 * Event triggered when delete params are built.
 */
class DeleteParamsEvent extends BaseParamsEvent {
}
