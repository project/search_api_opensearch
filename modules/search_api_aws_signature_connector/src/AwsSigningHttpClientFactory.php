<?php

declare(strict_types=1);

namespace Drupal\search_api_aws_signature_connector;

use Aws\Credentials\CredentialProvider;
use Aws\Credentials\Credentials;
use Aws\Exception\CredentialsException;
use Aws\Signature\SignatureInterface;
use Aws\Signature\SignatureV4;
use OpenSearch\Aws\SigningClientDecorator;
use OpenSearch\HttpClient\HttpClientFactoryInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * A decorator client that signs requests using the provided AWS credentials.
 */
class AwsSigningHttpClientFactory implements HttpClientFactoryInterface {

  public function __construct(
    protected HttpClientFactoryInterface $inner,
    protected ?SignatureInterface $signer = NULL,
    protected ?CredentialProvider $provider = NULL,
    protected ?LoggerInterface $logger = NULL,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function create(array $options): ClientInterface {
    // Create the inner client.
    $client = $this->inner->create($options);

    // Get the credentials.
    $provider = $this->getCredentialProvider($options);
    $promise = $provider();
    try {
      $credentials = $promise->wait();
    }
    catch (CredentialsException $e) {
      $this->logger?->error('Failed to get AWS credentials: @message', ['@message' => $e->getMessage()]);
      $credentials = new Credentials('', '');
    }

    // Get the signer.
    $signer = $this->getSigner($options);

    // Host header is required.
    $host = parse_url($options['base_uri'], PHP_URL_HOST);

    return new SigningClientDecorator($client, $credentials, $signer, ['host' => $host]);
  }

  /**
   * Gets the credential provider.
   *
   * @param array<string,mixed> $options
   *   The options array.
   */
  protected function getCredentialProvider(array $options): CredentialProvider|\Closure|null|callable {
    // Check for a provided credential provider.
    if ($this->provider) {
      return $this->provider;
    }

    // Check for provided API key and secret.
    if (isset($options['api_key']) && isset($options['api_secret'])) {
      return CredentialProvider::fromCredentials(
        new Credentials($options['api_key'], $options['api_secret'])
      );
    }

    // Fallback to the default provider.
    return CredentialProvider::defaultProvider();
  }

  /**
   * Gets the request signer.
   *
   * @param array<string,string> $options
   *   The options array.
   */
  protected function getSigner(array $options): SignatureInterface {
    if ($this->signer) {
      return $this->signer;
    }
    $service = $options['aws_service'] ?? 'es';
    $region = $options['aws_region'] ?? 'us-east-1';

    return new SignatureV4($service, $region);
  }

}
