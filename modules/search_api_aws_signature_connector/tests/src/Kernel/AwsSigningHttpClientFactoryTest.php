<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_aws_signature_connector\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\search_api_aws_signature_connector\Plugin\OpenSearch\Connector\AwsSignatureConnector;
use OpenSearch\Client;

/**
 * Tests the AWS signing connector.
 *
 * @group search_api_opensearch
 */
class AwsSigningHttpClientFactoryTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'search_api',
    'search_api_opensearch',
    'search_api_aws_signature_connector',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig([
      'search_api_opensearch',
      'search_api_aws_signature_connector',
    ]);
  }

  /**
   * Tests the data types.
   *
   * @group search_api_aws_signature_connector
   */
  public function testAwsSigningHttpClientFactory() {
    $pluginManager = \Drupal::service('plugin.manager.search_api_opensearch.connector');
    $connector = $pluginManager->createInstance('aws_signature');
    $this->assertInstanceOf(AwsSignatureConnector::class, $connector);
    $connector->setConfiguration([
      'url' => 'http://localhost:9200',
      'ssl_verification' => FALSE,
      'api_key' => 'test',
      'api_secret' => 'test',
      'aws_region' => 'us-east-1',
    ]);
    $client = $connector->getClient();
    $this->assertInstanceOf(Client::class, $client);
  }

}
